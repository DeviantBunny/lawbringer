using System.Collections.Generic;
using UnityEngine;

namespace Lawmaker
{
    public class EnemySpawner : MonoBehaviour
    {
        public SpawnerConfig spawnerConfig;
        public Transform spawnPoint;
        public List<SpriteRenderer> runes;
        public Color bloodRuneColor;

        private void Spawn(int tier)
        {
            var pos = spawnPoint.position;
            
            foreach (var item in spawnerConfig.spawnObjects)
            {
                var quantity = tier < item.quantities.Count ? item.quantities[tier] : item.quantities[^1];

                for (int i = 0; i < quantity ; i++)
                {
                    var newPos = pos + new Vector3(Random.Range(0, spawnerConfig.randomDistance),
                        Random.Range(0, spawnerConfig.randomDistance));
                
                    Instantiate(item.spawnObject, newPos, Quaternion.identity);
            
                    if (item.spawnEffect != null)
                    {
                        Instantiate(item.spawnEffect, newPos, Quaternion.identity);
                    }
                }
            }
        }

        public void StartRound(int tier)
        {
            Spawn(tier);
            foreach (var item in runes)
            {
                item.color = bloodRuneColor;
            }
        }

        public void EndRound()
        {
            foreach (var item in runes)
            {
                item.color = Color.white;
            }
        }
    }
}