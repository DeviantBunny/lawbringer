using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lawmaker
{
    public class DestroyOverTime : MonoBehaviour
    {
        public float time = 1f;
        private void Update()
        {
            if (time <= 0)
            {
                Destroy(gameObject);
            }
            time -= Time.deltaTime;
        }
    }
}