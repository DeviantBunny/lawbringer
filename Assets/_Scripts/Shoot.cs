using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject gunBarrel;
    public GameObject bullet;
    
    public void Fire()
    {
        Instantiate(bullet, gunBarrel.transform.position, gunBarrel.transform.rotation);
    }
}
