using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Lawmaker
{
    public class LevelDynamics : MonoBehaviour
    {
        public GameObject player;
        public List<EnemySpawner> altars;
        public Interact pillar;
        public List<Enemy> enemies;

        public delegate void StartRoundDelegate(int roundNum);
        public event StartRoundDelegate OnStartRound;

        public delegate void EndRoundDelegate();
        public event EndRoundDelegate OnEndRound;
        
        public  bool roundInProgress;
        
        private static LevelDynamics instance;
        private PlayerInputActions playerInputActions;
        private int playerHealth;
        private int roundNum;

        private InputAction restart;

        public static LevelDynamics Get()
        {
            return instance;
        }

        private void Awake()
        {
            instance = this;
            playerInputActions = new PlayerInputActions();
        }

        private void OnDestroy()
        {
            instance = null;
        }

        //TODO: Clean up events
        public void NewRound()
        {
            foreach (var item in altars)
            {
                item.StartRound(roundNum);
            }
            //pillar.StartRound();
            roundInProgress = true;
            roundNum++;
            OnStartRound?.Invoke(roundNum);
        }

        private void EndRound()
        {
            foreach (var item in altars)
            {
                item.EndRound();
            }
            pillar.EndRound();
            roundInProgress = false;
        }

        public void AddEnemy(Enemy enemy)
        {
            enemies.Add(enemy);
        }

        public void RemoveEnemy(Enemy enemy)
        {
            enemies.Remove(enemy);
            if (enemies.Count == 0) EndRound();
        }
        
        private static void OnRestart(InputAction.CallbackContext obj)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private void OnEnable()
        {
            restart = playerInputActions.UI.Restart;
            restart.performed += OnRestart;

            restart.Enable();
        }

        private void OnDisable()
        {
            restart.Disable();
        }
        
    }
}