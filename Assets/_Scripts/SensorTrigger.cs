
using UnityEngine;
using UnityEngine.Events;

namespace Lawmaker
{
    public class SensorTrigger : MonoBehaviour
    {
        public UnityEvent onEnter;
        public UnityEvent onExit;

        private void OnTriggerEnter2D(Collider2D other)
        {
            onEnter.Invoke();
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            onExit.Invoke();
        }
    }
}