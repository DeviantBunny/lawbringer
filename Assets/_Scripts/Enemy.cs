using UnityEngine;
using UnityEngine.Events;

namespace Lawmaker
{
    public class Enemy : MonoBehaviour
    {
        public float runSpeed;
        public float walkSpeed;
        public float recoveryTime = 0.2f;
        public GameObject sprite;
        public GameObject bloodSprite;
        public GameObject sensor;
        public float stoppingDistance;
        public float wakeUpTime = 2;
        public UnityEvent onAttack;
        public AudioSource hitAudioSource;
        public AudioSource screamAudioSource;

        private Transform playerTrans;
        private Animator anim;
        private Animator bloodAnim;
        private Timer timer;
        private Rigidbody2D body;
        

        private Vector2 direction;
        private float currentSpeed;
        private bool isActive;
        
        private void Start()
        {
            playerTrans = LevelDynamics.Get().player.transform;
            anim = sprite.GetComponent<Animator>();
            bloodAnim = bloodSprite.GetComponent<Animator>();
            body = GetComponent<Rigidbody2D>();

            timer = new Timer();
            timer.StartTimer(wakeUpTime, WakeUp);
            LevelDynamics.Get().AddEnemy(this);
        }

        private void WakeUp()
        {
            currentSpeed = runSpeed;
            isActive = true;
        }

        private void Update()
        {
            anim.SetBool("isMoving", body.velocity != Vector2.zero);
            
            if (direction.x > 0)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
            else if (direction.x < 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }

            timer.Update(Time.deltaTime);
        }

        public void Attack()
        {
            if (isActive)
            {
                anim.SetTrigger("attack");
                currentSpeed = walkSpeed;
                timer.StartTimer(recoveryTime, Recover);
                onAttack.Invoke();
            }
        }
        
        private void FixedUpdate()
        {
            direction = (playerTrans.position - transform.position).normalized;
            if (Vector2.Distance(playerTrans.position, transform.position) >= stoppingDistance)
            {
                body.velocity = direction * currentSpeed;
            }
            else
            {
                body.velocity = Vector2.zero;
            }
        }

        public void TakeHit()
        {
            anim.SetTrigger("takeHit");
            bloodAnim.SetTrigger("takeHit");
            hitAudioSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.BloodSplash);
            hitAudioSource.Play();
            
            currentSpeed = walkSpeed;
            body.velocity = Vector2.zero;
            timer.StartTimer(recoveryTime, Recover);
        }

        //TODO: Fade and destroy gameobject over time
        public void Death()
        {
            isActive = false;
            currentSpeed = 0f;
            body.velocity = Vector2.zero;
            anim.SetBool("death", true);
            bloodAnim.SetBool("death", true);
            GetComponent<CapsuleCollider2D>().enabled = false;
            LevelDynamics.Get().RemoveEnemy(this);
            
            screamAudioSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.Scream);
            screamAudioSource.Play();
            
        }

        public void Recover()
        {
            if (isActive)
            {
                currentSpeed = runSpeed;
            }
        }
        
    }
}