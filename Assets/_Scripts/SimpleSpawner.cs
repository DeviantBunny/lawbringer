using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lawmaker
{
    public class SimpleSpawner : MonoBehaviour
    {
        public GameObject spawnObject;
        public GameObject spawnEffect;
        public Transform spawnPoint;

        public void Spawn()
        {
            var pos = spawnPoint.position;
            Instantiate(spawnObject, pos, Quaternion.identity);
            
            if (spawnEffect != null)
            {
                Instantiate(spawnEffect, pos, Quaternion.identity);
            }
        }

    }
}