using System.Collections.Generic;
using UnityEngine;

public class AudioLibrary : MonoBehaviour
{
    private static AudioLibrary instance;
    public List<AudioClip> scream;
    public List<AudioClip> swordWhoosh;
    public List<AudioClip> swordScrape;
    public List<AudioClip> bloodSplash;
    public List<AudioClip> coltFire;
    public List<AudioClip> coltReload;
    public List<AudioClip> swordDraw;

    public static AudioLibrary Get()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }
    
    private void OnDestroy()
    {
        instance = null;
    }
    public AudioClip GetSoundClip(SoundCategory sound)
    {
        return sound switch
        {
            SoundCategory.SwordWhoosh => RandomizeSound(swordWhoosh),
            SoundCategory.SwordScrape => RandomizeSound(swordScrape),
            SoundCategory.Scream => RandomizeSound(scream),
            SoundCategory.BloodSplash => RandomizeSound(bloodSplash),
            SoundCategory.ColtFire => RandomizeSound(coltFire),
            SoundCategory.ColtReload => RandomizeSound(coltReload),
            SoundCategory.SwordDraw => RandomizeSound(swordDraw),
            _ => null
        };
    }
    public AudioClip RandomizeSound(List<AudioClip> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    public enum SoundCategory
    {
        Scream, 
        BloodSplash,
        ColtFire,
        ColtReload,
        SwordDraw,
        SwordWhoosh,
        SwordScrape
    }
}
