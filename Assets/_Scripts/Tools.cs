using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lawmaker
{
    public static class Tools
    {

        public static T TryGet<T>(this List<T> list, int index) where T : class
        {
            if (index >= 0 && index < list.Count)
            {
                return list[index];
            }
            else return null;
        }

        public static bool Between(this float value, float min, float max)
        {
            return (value >= min && value <= max);
        }

        public static bool Between(this int value, int min, int max)
        {
            return (value >= min && value <= max);
        }

        public static void RemoveNulls(List<GameObject> list)
        {
            list.RemoveAll(x => x == null);
        }

        public static bool IsEmpty<T>(this List<T> list)
        {
            return list.Count == 0;
        }
        
    }
}