using UnityEngine;

namespace Lawmaker
{
    public class SwordSlice : MonoBehaviour
    {
        public int damage;
        public AudioSource whooshSource;
        public AudioSource scrapeSource;

        private CapsuleCollider2D capsule;
        private Vector2 size;


        private void Awake()
        {
            capsule = GetComponent<CapsuleCollider2D>();
            size = capsule.size;
            capsule.size = new Vector2(0, 0);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.TryGetComponent(out Health health))
            {
                scrapeSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.SwordScrape);
                scrapeSource.Play();
                health.Damage(damage);
            }
        }

        public void Shrink()
        {
            capsule.size = new Vector2(0, 0);
        }

        public void Expand()
        {
            capsule.size = size;
            whooshSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.SwordWhoosh);
            whooshSource.Play();
        }
        
    }
}