using Unity.Mathematics;
using UnityEngine;

namespace Lawmaker
{
    public class Bullet : MonoBehaviour
    {
        public float speed = 20f;
        public int damage;
        public GameObject hitMarker;
        public LayerMask mask;

        private Rigidbody2D body;
        private Vector2 direction;
        private Vector2 currentPos;
        private Transform trans;

        private void Awake()
        {
            body = GetComponent<Rigidbody2D>();
            trans = transform;
            direction = new Vector2();
            direction = trans.up;
            currentPos = trans.position;
        }

        private void FixedUpdate()
        {
            body.velocity = direction * speed;

            RaycastHit2D hit = Physics2D.Raycast(currentPos, direction, Vector2.Distance(currentPos, trans.position),  mask);
            if (hit.collider != null)
            {
                Instantiate(hitMarker, hit.point, quaternion.identity);

                if (hit.collider.gameObject.TryGetComponent(out Health health))
                {
                    health.Damage(damage);
                }
                
                Destroy(gameObject);

            } 
            
            currentPos = trans.position;

        }
    }
}