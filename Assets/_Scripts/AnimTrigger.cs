using UnityEngine;
using UnityEngine.Events;

namespace Lawmaker
{
    public class AnimTrigger : MonoBehaviour
    {
        public UnityEvent triggerOn;
        public UnityEvent triggerOff;

        public void TriggerEventOn()
        {
            triggerOn.Invoke();
        }

        public void TriggerEventOff()
        {
            triggerOff.Invoke();
        }
    }
}