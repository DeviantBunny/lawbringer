using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Lawmaker
{
    [CreateAssetMenu(fileName = "SpawnerConfig", menuName = "ScriptableObjects/SpawnerConfig", order = 0)]
    public class SpawnerConfig : ScriptableObject
    {
        public float randomDistance = 1f;
        public List<SpawnObject> spawnObjects;
    }

    [Serializable]
    public class SpawnObject
    {
        public GameObject spawnObject;
        public GameObject spawnEffect;
        [ListDrawerSettings(ShowIndexLabels = true)]
        public List<int> quantities;
    }
}