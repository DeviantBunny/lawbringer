using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Lawmaker
{

    public class Timer
    {
        public float duration;
        public float elapsedTime;
        public bool isActive;
        private Action doneCallback;


        //Timer Constructors
        public Timer()
        {

        }

        public Timer(float duration)
        {
            TimerInit(duration);
        }

        public Timer(float duration, Action doneCallback)
        {
            TimerInit(duration, doneCallback);
        }

        //Timer Init
        public void TimerInit(float duration, Action doneCallback = null)
        {
            this.duration = duration;
            this.doneCallback = doneCallback;
            elapsedTime = 0;
        }


        //Time Cycle - Returns True if timer is finished
        public bool Update(float deltaTime)
        {

            if (isActive && !IsDone())
            {
                elapsedTime += deltaTime;
                if (IsDone() && doneCallback != null)
                {
                    doneCallback();
                }
            }

            return IsDone();
        }

        public bool IsDone()
        {
            return elapsedTime >= duration;
        }

        //Timer Functions
        public void StartTimer(float duration)
        {
            TimerInit(duration);
            isActive = true;
        }

        public void StartTimer(float duration, Action doneCallback)
        {
            TimerInit(duration, doneCallback);
            isActive = true;
        }

        public void PauseTimer()
        {
            isActive = false;
        }

        public void ResumeTimer()
        {
            isActive = true;
        }

        public float GetSeconds()
        {
            return elapsedTime;
        }

        public float GetProgress()
        {
            if (duration > 0)
            {
                return elapsedTime / duration;
            }
            else
            {
                return 0;
            }

        }

    }

}