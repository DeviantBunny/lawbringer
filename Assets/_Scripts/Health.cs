using UnityEngine;
using UnityEngine.Events;

namespace Lawmaker
{
    public class Health : MonoBehaviour
    {
        public int maxHealth;
        public UnityEvent onDeath;
        public UnityEvent onHit;
        public bool invulnerable;

        [SerializeField] private int currentHealth;
        private void Start()
        {
            currentHealth = maxHealth;
        }

        public void Damage(int damage)
        {
            if (invulnerable) return;
            currentHealth -= damage;
            onHit.Invoke();
            
            if (currentHealth <= 0)
            {
                onDeath.Invoke();
            }
        }

        public int GetCurrentHealth()
        {
            return currentHealth;
        }
    }
}