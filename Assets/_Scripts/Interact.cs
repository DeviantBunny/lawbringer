using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Lawmaker
{
    public class Interact : MonoBehaviour
    {
        public UnityEvent onInteract;
        public GameObject icon;
        public SpriteRenderer runeSprite;
        public Color bloodRuneColor;

        public AudioSource audioSource;

        private PlayerInputActions playerInputActions;
        private InputAction interact;
        
        private bool playerClose;

        private void Awake()
        {
            playerInputActions = new PlayerInputActions();
            LevelDynamics.Get().OnStartRound += OnStartRound;
        }

        //TODO: Clean up the events
        private void OnStartRound(int roundnum)
        {
            StartRound();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            playerClose = true;
            if (!LevelDynamics.Get().roundInProgress)
            {
                icon.SetActive(true);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            playerClose = false;
            icon.SetActive(false);
        }

        private void OnInteract(InputAction.CallbackContext obj)
        {
            if (playerClose && !LevelDynamics.Get().roundInProgress)
            {
                onInteract.Invoke();
                audioSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.SwordDraw);
                audioSource.Play();
            }
        }

        private void OnEnable()
        {
            interact = playerInputActions.Player.Interact;
            interact.performed += OnInteract;
            interact.Enable();
        }

        private void OnDisable()
        {
            interact.Disable();
        }

        public void StartRound()
        {
            runeSprite.color = bloodRuneColor;
        }
        public void EndRound()
        {
            runeSprite.color = Color.white;
        }
        
        
    }
}