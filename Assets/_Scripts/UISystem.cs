using System;
using System.Collections;
using System.Collections.Generic;
using Shapes;
using UnityEngine;
using UnityEngine.UI;

namespace Lawmaker
{
    public class UISystem : MonoBehaviour
    {
        private static UISystem instance;
        public PlayerController player;
        public Gun gun;
        public Disc ammoCounter;
        public Disc reloadTracker;
        public Line healthBar;

        private Animator ammoCounterAnim;
        private void Awake()
        {
            instance = this;
            ammoCounterAnim = ammoCounter.transform.parent.GetComponent<Animator>();
        }

        public void UpdateAmmoCounter(int ammo, Rotate value)
        {
            ammoCounter.DashSize = ammo;
            var radAngle = (float)(Math.PI * (ammo * 60) / 180);
            ammoCounter.AngRadiansEnd = radAngle;

            string rotTrigger = value == Rotate.Clockwise ? "rotateCW" : "rotateCCW";
            ammoCounterAnim.SetTrigger(rotTrigger);
        }

        public void UpdateReloadTracker(float percentage)
        {
            var radAngle = (float)(Math.PI * (percentage * 360) / 180);
            reloadTracker.AngRadiansEnd = radAngle;
        }

        public void UpdateHealthBar(float percentage)
        {
            healthBar.End = new Vector3(percentage, 0,0);
            healthBar.GetComponent<Animator>().SetTrigger("fade");
            healthBar.transform.parent.GetComponent<Animator>().SetTrigger("fade");
        }

        public static UISystem Get()
        {
            return instance;
        }

        private void OnDestroy()
        {
            instance = null;
        }
    }

    public enum Rotate
    {
        Clockwise,
        Counterclockwise
    }
}