using UnityEngine;

namespace Lawmaker
{
    public class Gun : MonoBehaviour
    {
        public int ammoCapacity;
        public float shootInterval;
        public float reloadTime;
        public int minDamage;
        public int maxDamage;
        public GameObject sprite;
        public AudioSource shootSource;
        public AudioSource reloadSource;

        private Shoot shoot;
        private Timer timer;
        [SerializeField]
        private int currentAmmo;

        private bool isReloading;
        private bool isInInterval;
        public bool isAiming;

        private void Awake()
        {
            shoot = GetComponent<Shoot>();
            timer = new Timer();
            currentAmmo = ammoCapacity;
        }

        private void Update()
        {
            timer.Update(Time.deltaTime);

            if (isReloading)
            {
                ToggleReloadTracker(true);
                // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
                UISystem.Get().UpdateReloadTracker(timer.GetProgress());
            }
            else
            {
                ToggleReloadTracker(false);
            }
            
            if (isAiming || isReloading)
            {
                ToggleSprite(true);
                ToggleAmmoCounter(true);
            }
            else
            {
                ToggleSprite(false);
                ToggleAmmoCounter(false);
            }

        }

        public void Fire()
        {
            if (isInInterval|| currentAmmo <= 0) return;
            currentAmmo--;
            UISystem.Get().UpdateAmmoCounter(currentAmmo, Rotate.Clockwise);
            shoot.Fire();
            isInInterval = true;
            timer.StartTimer(shootInterval, FinishInterval);
            
            shootSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.ColtFire);
            shootSource.Play();
        }

        private void FinishInterval()
        {
            isInInterval = false;
        }

        public void Reload()
        {
            if (currentAmmo < ammoCapacity)
            {
                timer.StartTimer(reloadTime, FinishReload);
                isReloading = true;
                ToggleSprite(true);
                
                reloadSource.clip = AudioLibrary.Get().GetSoundClip(AudioLibrary.SoundCategory.ColtReload);
                reloadSource.Play();
            }
            else
            {
                isReloading = false;
            }
        }

        // ReSharper disable Unity.PerformanceAnalysis
        public void FinishReload()
        {
            if (isReloading)
            {
                currentAmmo++;
                UISystem.Get().UpdateAmmoCounter(currentAmmo, Rotate.Counterclockwise);
                Reload();
            }
            else
            {
                ToggleSprite(false);
            }
        }

        public void InterruptReload()
        {
            if (isReloading)
            {
                timer.PauseTimer();
                isReloading = false;
            }
        }
        
        public void ToggleSprite(bool value)
        {
            sprite.SetActive(value);

        }

        //TODO: Move this to UISystem
        public void ToggleAmmoCounter(bool value)
        {
            UISystem.Get().ammoCounter.transform.parent.gameObject.SetActive(value);
        }

        public void ToggleReloadTracker(bool value)
        {
            UISystem.Get().reloadTracker.gameObject.SetActive(value);
        }
        
    }
}