using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Lawmaker
{
    public class PlayerController : MonoBehaviour
    {
        public float runSpeed = 6f;
        public float walkSpeed = 4f;
        public float dodgeSpeed = 10f;
        public float dodgeDuration = 0.5f;
        public float meleeAttackDuration = 0.2f;
        public float followupWindowMin = 0.3f;
        public float followupWindowMax = 0.5f;
        public Animator animator;
        public Animator bloodAnim;
        public Gun gun;
        public GameObject echo;
        public GameObject swordCollider;
        public float echoFrequency;
        public AudioSource audioSource;

        private Rigidbody2D body;
        private Timer attackTimer;
        private Timer dodgeTimer;

        private PlayerInputActions playerInputActions;
        private InputAction movement;
        private InputAction attack;
        private InputAction dodge;
        private InputAction aim;
        private InputAction mousePosition;
        private InputAction reload;
        private Health health;
        private GameObject swordInstance;

        private Vector2 moveDirection;
        private float moveSpeed;

        private bool isDodging;
        private bool isAiming;
        private bool isAttacking;
        private bool isLeft;
        private float timeToSpawn;
        
        private void Awake()
        {
            playerInputActions = new PlayerInputActions();
            body = GetComponent<Rigidbody2D>();
            attackTimer = new Timer();
            dodgeTimer = new Timer();
            
            moveSpeed = runSpeed;
            health = GetComponent<Health>();
        }

        private Vector2 GetDirectionToMouse()
        {
            return Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());
        }
        
        private void Update()
        {
            // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
            moveDirection = movement.ReadValue<Vector2>();
            attackTimer.Update(Time.deltaTime);
            dodgeTimer.Update(Time.deltaTime);

            //Control Movement
            animator.SetBool("isMoving", moveDirection != Vector2.zero);
            if (moveDirection.x > 0)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
            else if (moveDirection.x < 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            
            if (isDodging)
            {
                if (timeToSpawn <= 0)
                {
                    Instantiate(echo, transform.position, quaternion.identity);
                    timeToSpawn = echoFrequency;
                }
                else
                {
                    timeToSpawn -= Time.deltaTime;
                }
            }

            if (isAiming)
            {
                gun.transform.up = new Vector2(GetDirectionToMouse().x - transform.position.x, GetDirectionToMouse().y - transform.position.y) ;
            }
        }

        private void FixedUpdate()
        {
            body.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
        }

        private void OnDodge(InputAction.CallbackContext obj)
        {
            gun.InterruptReload();
            if (IsBusy()) return;

            if (moveDirection != Vector2.zero)
            {
                dodgeTimer.StartTimer(dodgeDuration, FinishDodge);
                moveSpeed = dodgeSpeed;
                animator.SetTrigger("dodge");
                health.invulnerable = true;
                isDodging = true;
            }
        }
        
        private void FinishDodge()
        {
            moveSpeed = runSpeed;
            health.invulnerable = false;
            isDodging = false;
        }


        
        private void OnAimStart(InputAction.CallbackContext obj)
        {
            if (IsBusy()) return;
            
            gun.ToggleSprite(true);
            moveSpeed = walkSpeed;
            isAiming = true;
            gun.isAiming = true;
        }
        
        private void OnAimStop(InputAction.CallbackContext obj)
        {
            if (isAiming)
            {
                moveSpeed = runSpeed;
                isAiming = false;
                gun.isAiming = false;
            }
        }

        private void OnAttack(InputAction.CallbackContext obj)
        {
            gun.InterruptReload();

            if (isAiming)
            {
                gun.Fire();
                return;
            }
            MeleeAttack();
        }

        private void MeleeAttack()
        {
            if (isAttacking && attackTimer.GetSeconds() > followupWindowMin && attackTimer.GetSeconds() < followupWindowMax)
            {
                animator.SetTrigger("followup");
                attackTimer.StartTimer(meleeAttackDuration, FinishAttack);
            }
            else if (!isAttacking)
            {
                animator.SetTrigger("attack");
                isAttacking = true;
                if (!isDodging)
                {
                    moveSpeed = walkSpeed;
                }
                attackTimer.StartTimer(meleeAttackDuration, FinishAttack);
            }
        }

        public void StartMelee()
        {
            isAttacking = true;
        }

        public void FinishMelee()
        {
            isAttacking = false;

        }

        void FinishAttack()
        {
            isAttacking = false;
            moveSpeed = runSpeed;
        }
        
        private void OnReload(InputAction.CallbackContext obj)
        {
            if (IsBusy()) return;
            gun.Reload();
        }
        private bool IsBusy()
        {
            return isAttacking || isDodging;
        }
        
        public void TakeHit()
        {
            animator.SetTrigger("takeHit");
            bloodAnim.SetTrigger("takeHit");
            UISystem.Get().UpdateHealthBar((float)health.GetCurrentHealth() / health.maxHealth);
        }

        public void Death()
        {
            animator.SetTrigger("death");
            bloodAnim.SetTrigger("death");
            
            //Shuts down controls after player death
            movement.Disable();
            attack.Disable();
            dodge.Disable();
            aim.Disable();

            //This prevents future hit animations. Find a better solution
            health.invulnerable = true;
        }

        //Player Input Actions
        private void OnEnable()
        {
            movement = playerInputActions.Player.Move;
            attack = playerInputActions.Player.Attack;
            dodge = playerInputActions.Player.Dodge;
            aim = playerInputActions.Player.Aim;
            reload = playerInputActions.Player.Reload;
            mousePosition = playerInputActions.Player.Look;
            
            attack.performed += OnAttack;
            dodge.performed += OnDodge;
            aim.started += OnAimStart;
            aim.canceled += OnAimStop;
            reload.performed += OnReload;

            movement.Enable();
            attack.Enable();
            dodge.Enable();
            aim.Enable();
            reload.Enable();
            mousePosition.Enable();
        }

        private void OnDisable()
        {
            movement.Disable();
            attack.Disable();
            dodge.Disable();
            aim.Disable();
            reload.Disable();
            mousePosition.Disable();
        }

    }
}